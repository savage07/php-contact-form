<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Lead;
use App\Mail\LeadNotice;

class LeadController extends Controller
{
    public function create(Request $request, Lead $lead, LeadNotice $leadNotice)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        $newLead = $lead::create($request->all());
        
        Mail::to('guy-smiley@example.com')->send(new $leadNotice($newLead));

        return redirect('/thankyou');
    }
}
