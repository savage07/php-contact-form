<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Lead;
use App\Mail\LeadNotice;
use Illuminate\Support\Facades\Mail;

class SaveLeadTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSaveToDBAndSendEmailNotification()
    {
        $lead = [
            'name' => 'Adam',
            'email' => 'a@b.com',
            'phone' => '(908) 123-4567',
            'message' => 'hi there...'
        ];

        Mail::fake();

        $newLead = Lead::create($lead);
        
        Mail::to('guy-smiley@example.com')->send(new LeadNotice($newLead));

        Mail::assertSent(LeadNotice::class, 1);

        $this->assertDatabaseHas(
            'leads',
            $lead
        );
    }
}
