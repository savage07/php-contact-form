<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LeadFormSubmissionTest extends DuskTestCase
{
    use RefreshDatabase;

    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testFormSubmissionWithValidData()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->type('name', 'Adam')
                    ->type('email', 'adam@gm.com')
                    ->type('phone', '(908) 123-4567')
                    ->type('message', 'hi there')
                    ->click('#leadForm button')
                    ->assertPathIs('/thankyou');

            $this->assertDatabaseHas('leads', [
                'name' => 'Adam',
                'email' => 'adam@gm.com',
                'phone' => '(908) 123-4567',
                'message' => 'hi there'
            ]);
        });
    }

    public function testFormSubmissionFailureWithInvalidEmail()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->type('name', 'Adam')
                    ->type('email', 'adamgm.com')
                    ->type('phone', '(908) 123-4567')
                    ->type('message', 'hi there')
                    ->click('#leadForm button')
                    ->pause(1000)
                    ->assertPathIs('/');

            $this->assertDatabaseMissing('leads', [
                'name' => 'Adam',
                'email' => 'adamgm.com',
                'phone' => '(908) 123-4567',
                'message' => 'hi there'
            ]);
        });    
    }

    public function testFormSubmissionFailureWithAbsentName()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->type('name', '')
                    ->type('email', 'adamgm.com')
                    ->type('phone', '(908) 123-4567')
                    ->type('message', 'hi there')
                    ->click('#leadForm button')
                    ->pause(1000)
                    ->assertPathIs('/');

            $this->assertDatabaseMissing('leads', [
                'name' => '',
                'email' => 'adamgm.com',
                'phone' => '(908) 123-4567',
                'message' => 'hi there'
            ]);     
        });    
    }
}
