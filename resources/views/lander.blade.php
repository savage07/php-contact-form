    @extends('layouts.app')

    @section('main')

    @include('lander.nav')

    @include('lander.intro')

    @include('lander.about')

    @include('lander.contact')

    @include('lander.map')

    @endsection