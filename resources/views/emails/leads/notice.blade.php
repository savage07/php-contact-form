<h1>New Lead</h1>

<p>
  <strong>Name:</strong> {{ $lead->name }}
</p>
<p>
  <strong>Email:</strong> {{ $lead->email }}
</p>
<p>
  <strong>Phone:</strong> {{ $lead->phone }}
</p>
<p>
  <strong>Message:</strong>
</p>

<p>{{ $lead->message }}</p>