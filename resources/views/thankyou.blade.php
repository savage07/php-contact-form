    @extends('layouts.app')

    @section('main')

    <!-- Offer -->

    <section id="offer" class="container content-section text-center">

        <p>Guy Smiley will be with you via email in a minute. In the mean time he has a special <u><strong>ONE TIME OFFER FOR YOU</strong></u></p>

        <h3>Get <u><strong>50% OFF</strong></u> Guy Smiley's Public Speaking Master Class</h3>

        <img class="img-fluid" alt="Guy Smiley Headshot" src="img/guy-smiley-bw.jpg">

        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

          <!-- Identify your business so that you can collect the payments. -->
          <input type="hidden" name="business" value="mcmahonempire@gmail.com">

          <!-- Specify a Buy Now button. -->
          <input type="hidden" name="cmd" value="_xclick">

          <!-- Specify details about the item that buyers will purchase. -->
          <input type="hidden" name="item_name" value="Pay me $299.95 for absolutely nothing. NO REFUNDS!">
          <input type="hidden" name="amount" value="299.95">
          <input type="hidden" name="currency_code" value="USD">

          <!-- Display the payment button. -->
          <button class="btn btn-default animated swing">BECOME A PUBLIC SPEAKING MASTER NOW</button>
          <img alt="" border="0" width="1" height="1"
          src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

        </form>

    </section>

    @endsection