
    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <h2>Free Public Speaking Course</h2>
            <p>Get Guy Smiley's Free Course and Become an Awesome Public Speaker.</p>
                @if($errors->any())
                    <div class="alert alert-block alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form id="leadForm" action="{{ url('/leads') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" name="name" value="{{ old('name') }}" placeholder="FULL NAME *" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" value="{{ old('email') }}" placeholder="EMAIL *" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <input type="text" name="phone" value="{{ old('phone') }}" placeholder="PHONE" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="message" class="form-control" placeholder="Why do you want to be a great public speaker?" required>{{ old('message') }}</textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-default btn-block animated infinite pulse" type="submit">BECOME AN AWESOME PUBLIC SPEAKER NOW!</button>
                    </div>
                </form>
                <div class="clearfix"></div>
                <br>
            </div>
        </div>
    </section>