    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Relate to prospect.</h2>
                <p>Have you ever been yatta yatta yatta...</p>
                <p>I was just like you when...</p>
            </div>
        </div>
    </section>

    <section id="coffee" class="content-section text-center">
        <div class="download-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>Still not convinced?</h2>
                    <p>Witness the elegance of Guy Smiley's SH Dinner Speech.</p>
                    <a target="_blank" href="https://www.youtube.com/watch?v=PnrjzFvhFI4" class="btn btn-default btn-lg">watch SH Dinner on Youtube</a>
                </div>
            </div>
        </div>
    </section>